package pracovniList;

public class Pracovnik {

    private boolean aktivni;
    private String krestni_jmeno;
    private String prijmeni;

    public void setAktivni(boolean aktivni) {
        this.aktivni = aktivni;
    }

    public boolean getAktivni() {
        return aktivni;
    }

    public void setKrestni_jmeno(String krestni_jmeno) {
        this.krestni_jmeno = krestni_jmeno;
    }

    public String getKrestni_jmeno() {
        return krestni_jmeno;
    }   // getKrestni_jmeno()   ((( getKrestni DAVALO ERROR )))

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

}
