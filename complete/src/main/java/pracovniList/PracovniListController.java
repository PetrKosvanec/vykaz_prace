package pracovniList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PracovniListController {

    @GetMapping("/pracovnik")
    public String pracovnikForm(Model model) {
        model.addAttribute("pracovnik", new Pracovnik());
        return "pracovnik";
    }

    @PostMapping("/pracovnik")
    public String pracovnikSubmit(@ModelAttribute Pracovnik pracovnik) {
        return "pracovnikResult";
    }

}
